package melnikacg.notebook.reminder;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import melnikacg.notebook.R;
import melnikacg.notebook.database.DbProviderMetaData;
import melnikacg.notebook.ui.PagerActivity;

import static melnikacg.notebook.model.Note.NOTE_ID_EXTRA;

/**
 * IntentService extension class for reminder handling.
 */
public class AlarmService extends IntentService {

    private String mTitle;
    private int mNoteId;

    public AlarmService() {
        super("AlarmService");
    }

    @Override
    protected final void onHandleIntent(Intent intent) {
        playRingtone();
        sendNotification();
        mNoteId = intent.getIntExtra(DbProviderMetaData.NoteTableMetaData.COLUMN_ID, 0);
        mTitle = intent.getStringExtra(DbProviderMetaData.NoteTableMetaData.COLUMN_TITLE);
    }

    private void sendNotification() {
        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, PagerActivity.class);
        intent.putExtra(NOTE_ID_EXTRA, mNoteId);

        PendingIntent contentIntent = PendingIntent
                .getActivity(this, 0, intent, 0);

        NotificationCompat.Builder alarmNotificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Alarm").setSmallIcon(R.mipmap.ic_launcher)
                .setStyle(new NotificationCompat.BigTextStyle().bigText("Notebook notification"))
                .setContentText(mTitle)
                .setAutoCancel(true)
                .setContentIntent(contentIntent);

        notificationManager.notify(1, alarmNotificationBuilder.build());
    }

    private void playRingtone() {
        Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone ringtone = RingtoneManager.getRingtone(this, alarmUri);
        ringtone.play();
    }
}
