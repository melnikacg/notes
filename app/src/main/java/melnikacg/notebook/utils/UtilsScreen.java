package melnikacg.notebook.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

/**
 * Screen utility class.
 */
public final class UtilsScreen {

    private static final int TWO_COLUMNS = 2;
    private static final int THREE_COLUMNS = 3;

    private UtilsScreen() {
    }

    private static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
        boolean large = ((context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    /**
     * @param activity {@link Activity} instance.
     * @return int columns to be displayed.
     */
    public static int getDisplayColumns(Activity activity) {
        int columnCount = TWO_COLUMNS;
        if (isTablet(activity)) {
            columnCount = THREE_COLUMNS;
        }
        return columnCount;
    }
}
