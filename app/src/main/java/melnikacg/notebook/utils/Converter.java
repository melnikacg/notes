package melnikacg.notebook.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

public final class Converter {

    private Converter() {
    }

    private static final SimpleDateFormat SDF = new SimpleDateFormat(
            "d MMM., HH:mm", Locale.getDefault());

    public static String getFormattedDate(long date) {
        return SDF.format(date);
    }
}
