package melnikacg.notebook.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * DatabaseHelper class contains some methods for SQLiteOpenHelper extension handling.
 */
final class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "notes.db";
    private static final int DB_VER = 1;
    private static DatabaseHelper mInstance = null;

    private DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VER);
    }

    private static synchronized DatabaseHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    static synchronized SQLiteDatabase getDb(Context context) {
        return getInstance(context).getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createDatabase(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            deleteDatabase(db);
            createDatabase(db);
        }
    }

    private void createDatabase(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + DbProviderMetaData.NoteTableMetaData.TABLE_NAME +
                " (" +
                DbProviderMetaData.NoteTableMetaData.COLUMN_ID + " INTEGER primary key," +
                DbProviderMetaData.NoteTableMetaData.COLUMN_TITLE + " TEXT," +
                DbProviderMetaData.NoteTableMetaData.COLUMN_MESSAGE + " TEXT," +
                DbProviderMetaData.NoteTableMetaData.COLUMN_DATE_CREATE + " INTEGER," +
                DbProviderMetaData.NoteTableMetaData.COLUMN_DATE_REMIND + " INTEGER)");
    }

    private void deleteDatabase(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + DbProviderMetaData.NoteTableMetaData.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS _iceboxes");
    }
}
