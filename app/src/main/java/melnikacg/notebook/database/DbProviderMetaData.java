package melnikacg.notebook.database;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * MetaData class.
 */
public final class DbProviderMetaData {

    private DbProviderMetaData() {
    }

    static final String AUTHORITY = "melnikacg.notebook.database.DbProvider";

    /**
     * Note table constants.
     */
    public static final class NoteTableMetaData implements BaseColumns {
        static final String TABLE_NAME = "notes";
        static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.notes.note";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/notes");
        static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.notes.note";

        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_MESSAGE = "message";
        public static final String COLUMN_DATE_CREATE = "date_create";
        public static final String COLUMN_DATE_REMIND = "date_remind";

        public static final String DEFAULT_SORT_ORDER_CREATE = COLUMN_DATE_CREATE + " ASC";
        public static final String REVERSE_SORT_ORDER_CREATE = COLUMN_DATE_CREATE + " DESC";
        public static final String DEFAULT_SORT_ORDER_REMIND = COLUMN_DATE_REMIND + " ASC";
        public static final String REVERSE_SORT_ORDER_REMIND = COLUMN_DATE_REMIND + " DESC";

    }

}
