package melnikacg.notebook.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import melnikacg.notebook.database.DbProviderMetaData.NoteTableMetaData;

import java.util.HashMap;
import java.util.Map;

/**
 * ContentProvider class for  SQLite database.
 */
public class DbProvider extends ContentProvider {

    private static final int ALL_NOTES = 1;
    private static final int SINGLE_NOTE = 2;
    private static UriMatcher uriMatcher;
    private static Map<String, String> notesProjectionMap;

    static {

        notesProjectionMap = new HashMap<>();
        notesProjectionMap.put(NoteTableMetaData._ID, NoteTableMetaData._ID);
        notesProjectionMap.put(NoteTableMetaData.COLUMN_TITLE, NoteTableMetaData.COLUMN_TITLE);
        notesProjectionMap.put(NoteTableMetaData.COLUMN_MESSAGE, NoteTableMetaData.COLUMN_MESSAGE);
        notesProjectionMap.put(NoteTableMetaData.COLUMN_DATE_CREATE, NoteTableMetaData.COLUMN_DATE_CREATE);
        notesProjectionMap.put(NoteTableMetaData.COLUMN_DATE_REMIND, NoteTableMetaData.COLUMN_DATE_REMIND);

        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(DbProviderMetaData.AUTHORITY, "notes", ALL_NOTES);
        uriMatcher.addURI(DbProviderMetaData.AUTHORITY, "notes/#", SINGLE_NOTE);
    }

    private SQLiteDatabase db;

    @Override
    public final boolean onCreate() {
        db = DatabaseHelper.getDb(getContext());
        return true;
    }

    @Nullable
    @Override
    public final Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs,
                              String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        switch (uriMatcher.match(uri)) {
            case ALL_NOTES:
                queryBuilder.setTables(DbProviderMetaData.NoteTableMetaData.TABLE_NAME);
                queryBuilder.setProjectionMap(notesProjectionMap);
                break;
            case SINGLE_NOTE:
                queryBuilder.setTables(DbProviderMetaData.NoteTableMetaData.TABLE_NAME);
                queryBuilder.setProjectionMap(notesProjectionMap);
                queryBuilder.appendWhere(DbProviderMetaData.NoteTableMetaData._ID +
                        "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        Cursor c = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        if (getContext() != null) {
            c.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return c;
    }

    @Nullable
    @Override
    public final String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case ALL_NOTES:
                return DbProviderMetaData.NoteTableMetaData.CONTENT_TYPE;
            case SINGLE_NOTE:
                return DbProviderMetaData.NoteTableMetaData.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Nullable
    @Override
    public final Uri insert(@NonNull Uri uri, ContentValues values) {
        Uri insertedUri;
        long rowId = db.insert(DbProviderMetaData.NoteTableMetaData.TABLE_NAME, null,
                values);
        if (rowId == -1) {
            throw new SQLiteException("Failed to insert row into " + uri);
        }
        insertedUri = ContentUris
                .withAppendedId(DbProviderMetaData.NoteTableMetaData.CONTENT_URI,
                        rowId);
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(insertedUri, null);
        }
        return insertedUri;
    }

    @Override
    public final int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int rowsDeleted;
        switch (uriMatcher.match(uri)) {
            case ALL_NOTES:
                rowsDeleted = db.delete(DbProviderMetaData.NoteTableMetaData.TABLE_NAME, selection,
                        selectionArgs);
                break;
            case SINGLE_NOTE:
                rowsDeleted = db.delete(DbProviderMetaData.NoteTableMetaData.TABLE_NAME,
                        NoteTableMetaData.COLUMN_ID + "=" + uri.getPathSegments().get(1),
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public final int update(@NonNull Uri uri, ContentValues values, String selection,
                            String[] selectionArgs) {
        String id = uri.getPathSegments().get(1);
        int count = db.update(DbProviderMetaData.NoteTableMetaData.TABLE_NAME, values,
                DbProviderMetaData.NoteTableMetaData._ID + "=" + id, selectionArgs);

        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }
}
