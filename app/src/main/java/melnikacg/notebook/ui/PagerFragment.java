package melnikacg.notebook.ui;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import melnikacg.notebook.R;
import melnikacg.notebook.database.DbProviderMetaData;
import melnikacg.notebook.model.Note;
import melnikacg.notebook.utils.Converter;

/**
 * A placeholder fragment containing a simple view.
 */
public class PagerFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * The fragment argument representing the section number for this fragment.
     */
    private static final String ARG_NOTE_ID = "note_id";

    private TextView mNoteTitle;
    private TextView mNoteMessage;
    private TextView mNoteReminder;
    private Note mNote;

    public PagerFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section number.
     */
    public static PagerFragment newInstance(int noteId) {
        PagerFragment fragment = new PagerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_NOTE_ID, noteId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_note, container, false);

        mNoteTitle = (TextView) rootView.findViewById(R.id.note_title);
        mNoteMessage = (TextView) rootView.findViewById(R.id.note_message);
        mNoteReminder = (TextView) rootView.findViewById(R.id.tvNoteReminder);

        getLoaderManager().initLoader(0, getArguments(), this);
        return rootView;
    }

    @Override
    public final Loader<Cursor> onCreateLoader(int id, Bundle args) {
        int noteId = args.getInt(ARG_NOTE_ID);

        return new CursorLoader(getActivity(),
                Uri.withAppendedPath(DbProviderMetaData.NoteTableMetaData.CONTENT_URI,
                        String.valueOf(noteId)), null, null, null, null);
    }

    @Override
    public final void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            cursor.moveToFirst();
            mNote = Note.cursorToNote(cursor);
            fillViews();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    private void fillViews() {
        mNoteTitle.setText(mNote.getTitle());
        mNoteMessage.setText(mNote.getMessage());

        if (mNote.getDateRemind() > 0L) {
            mNoteReminder.setText(Converter.getFormattedDate(mNote.getDateRemind()));
            mNoteReminder.setVisibility(View.VISIBLE);
        } else {
            mNoteReminder.setVisibility(View.GONE);
        }
    }

    /**
     * @return Note instance.
     */
    public final Note getNote() {
        return mNote;
    }

}
