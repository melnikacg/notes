package melnikacg.notebook.ui;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import melnikacg.notebook.R;
import melnikacg.notebook.database.DbProviderMetaData.NoteTableMetaData;
import melnikacg.notebook.model.Note;
import melnikacg.notebook.reminder.AlarmReceiver;
import melnikacg.notebook.utils.Converter;

/**
 * CreateEditNoteActivity class.
 */
public class CreateEditNoteActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String NEW_NOTE_EXTRA = "new_note";

    private AlarmManager mAlarmManager;
    private EditText mEtNoteTitle;
    private EditText mEtNoteMessage;
    private TextView mTvNoteReminder;
    private boolean mIsNewNote = false;
    private Note mNote;
    private long mReminder = 0L;
    private Calendar mTempCalendar;

    public static void createNewNote(Context context) {
        Intent intent = new Intent(context, CreateEditNoteActivity.class);
        intent.putExtra(NEW_NOTE_EXTRA, true);
        context.startActivity(intent);
    }

    public static void editNote(Context context, Note note) {
        Intent intent = new Intent(context, CreateEditNoteActivity.class);
        intent.putExtra(NEW_NOTE_EXTRA, false);
        intent.putExtra(Note.class.getCanonicalName(), note);
        context.startActivity(intent);
    }

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_note);

        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        mEtNoteTitle = (EditText) findViewById(R.id.etNoteTitle);
        mEtNoteMessage = (EditText) findViewById(R.id.etNoteMessage);
        mTvNoteReminder = (TextView) findViewById(R.id.tvNoteReminder);

        //populate widgets with note data
        mIsNewNote = getIntent().getBooleanExtra(CreateEditNoteActivity.NEW_NOTE_EXTRA, true);
        //}
        if (!mIsNewNote) {
            mNote = getIntent().getParcelableExtra(Note.class.getCanonicalName());
            mEtNoteTitle.setText(mNote.getTitle());
            mEtNoteMessage.setText(mNote.getMessage());

            if (mNote.getDateRemind() > 0L) {
                mReminder = mNote.getDateRemind();
                mTvNoteReminder.setText(Converter.getFormattedDate(mNote.getDateRemind()));
                mTvNoteReminder.setVisibility(View.VISIBLE);
            } else {
                mTvNoteReminder.setVisibility(View.GONE);
            }
            mTvNoteReminder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addOrEditReminder();
                }
            });
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsNewNote) {
                    Uri inserted = getContentResolver().insert(NoteTableMetaData.CONTENT_URI,
                            Note.createNoteCv(mEtNoteTitle.getText() + "",
                                    mEtNoteMessage.getText() + "", mReminder));
                    createNewNoteReminder(inserted);
                } else {
                    getContentResolver().update(Uri.withAppendedPath(NoteTableMetaData.CONTENT_URI,
                            String.valueOf(mNote.getNoteId())),
                            Note.createNoteCv(mNote.getNoteId(), mEtNoteTitle.getText()
                                    + "", mEtNoteMessage.getText() + "", mReminder), null, null);
                    setReminderAlarm(mNote.getNoteId());
                }
                startActivity(new Intent(CreateEditNoteActivity.this, MainActivity.class));
            }
        });
        setToolbarForActivity();
    }

    @Override
    public final boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_edit, menu);
        return true;
    }

    @Override
    public final boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem removeReminderItem = menu.findItem(R.id.action_remove_reminder);
        removeReminderItem.setVisible(mTvNoteReminder.getVisibility() == View.VISIBLE);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public final boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_delete_note:
                removeReminderAlarm(mNote.getNoteId());
                getContentResolver().delete(Uri.withAppendedPath(NoteTableMetaData.CONTENT_URI,
                        String.valueOf(mNote.getNoteId())), null, null);

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_set_reminder:
                addOrEditReminder();
                return true;
            case R.id.action_remove_reminder:
                mReminder = 0L;
                mTvNoteReminder.setText("");
                mTvNoteReminder.setVisibility(View.GONE);
                invalidateOptionsMenu();
                removeReminderAlarm(mNote.getNoteId());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setToolbarForActivity() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.createEditNoteToolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateEditNoteActivity.this, MainActivity.class);
                CreateEditNoteActivity.this.startActivity(intent);
                CreateEditNoteActivity.this.finish();
            }
        });
    }

    private void addOrEditReminder() {
        mTempCalendar = Calendar.getInstance();
        if (mReminder > 0L) {
            mTempCalendar.setTimeInMillis(mReminder);
        }

        new DatePickerDialog(this, this, mTempCalendar.get(Calendar.YEAR),
                mTempCalendar.get(Calendar.MONTH), mTempCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @Override
    public final void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mTempCalendar.set(Calendar.YEAR, year);
        mTempCalendar.set(Calendar.MONTH, month);
        mTempCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        new TimePickerDialog(this, this, mTempCalendar.get(Calendar.HOUR_OF_DAY),
                mTempCalendar.get(Calendar.MINUTE), true).show();
    }

    @Override
    public final void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mTempCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mTempCalendar.set(Calendar.MINUTE, minute);

        mReminder = mTempCalendar.getTimeInMillis();

        mTvNoteReminder.setText(Converter.getFormattedDate(mReminder));
        mTvNoteReminder.setVisibility(View.VISIBLE);
    }

    private void createNewNoteReminder(Uri insertedUri) {
        int id = Integer.parseInt(insertedUri.getLastPathSegment());
        setReminderAlarm(id);
    }

    /**
     * New note and existing note reminder can be updated.
     *
     * @param id note id.
     */
    private void setReminderAlarm(int id) {
        if (mReminder == 0L) {
            return;
        }
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(NoteTableMetaData.COLUMN_ID, id);
        intent.putExtra(NoteTableMetaData.COLUMN_TITLE, mEtNoteTitle.getText());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        mAlarmManager.set(AlarmManager.RTC, mReminder, pendingIntent);
    }

    /**
     * Only existing note reminder can be removed.
     *
     * @param id note id.
     */
    private void removeReminderAlarm(int id) {
        if (mIsNewNote) {
            return;
        }
        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        mAlarmManager.cancel(pendingIntent);
        pendingIntent.cancel();
    }
}
