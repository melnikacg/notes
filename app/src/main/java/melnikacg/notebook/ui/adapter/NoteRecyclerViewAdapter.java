package melnikacg.notebook.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import melnikacg.notebook.R;
import melnikacg.notebook.model.Note;
import melnikacg.notebook.utils.Converter;

public class NoteRecyclerViewAdapter extends RecyclerView.Adapter<NoteViewHolder>
        implements View.OnClickListener {

    private List<Note> mItemList;
    private OnItemClickListener mOnItemClickListener;

    public NoteRecyclerViewAdapter() {
        mItemList = new ArrayList<>();
    }

    public final void setItems(List<Note> itemList) {
        mItemList = itemList;
        notifyDataSetChanged();
    }

    @Override
    public final NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.solvent_list,
                null);
        layoutView.setOnClickListener(this);
        return new NoteViewHolder(layoutView);
    }

    @Override
    public final void onBindViewHolder(NoteViewHolder holder, int position) {
        String title = mItemList.get(position).getTitle();
        if (title != null && !title.isEmpty()) {
            holder.noteTitle.setText(title);
            holder.noteTitle.setVisibility(View.VISIBLE);
        } else {
            holder.noteTitle.setVisibility(View.GONE);
        }

        holder.noteText.setText(mItemList.get(position).getMessage());

        long reminder = mItemList.get(position).getDateRemind();
        if (reminder > 0L) {
            holder.noteReminder.setText(Converter.getFormattedDate(reminder));
            holder.noteReminder.setVisibility(View.VISIBLE);
        } else {
            holder.noteReminder.setVisibility(View.GONE);
        }

        holder.itemView.setTag(mItemList.get(position).getNoteId());
    }

    @Override
    public final int getItemCount() {
        return this.mItemList.size();
    }

    public final void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public final void onClick(View v) {
        mOnItemClickListener.onItemClick((int) v.getTag());
    }

    public interface OnItemClickListener {
        void onItemClick(int noteID);
    }
}
