package melnikacg.notebook.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import melnikacg.notebook.R;

/**
 * NoteViewHolder class.
 */
class NoteViewHolder extends RecyclerView.ViewHolder {

    TextView noteTitle;
    TextView noteText;
    TextView noteReminder;

    NoteViewHolder(View itemView) {
        super(itemView);
        noteTitle = (TextView) itemView.findViewById(R.id.note_title);
        noteText = (TextView) itemView.findViewById(R.id.note_message);
        noteReminder = (TextView) itemView.findViewById(R.id.note_reminder);
    }

}
