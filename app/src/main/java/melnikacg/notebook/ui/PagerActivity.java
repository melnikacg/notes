package melnikacg.notebook.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import melnikacg.notebook.R;
import melnikacg.notebook.database.DbProviderMetaData;
import melnikacg.notebook.model.Note;

import static melnikacg.notebook.model.Note.NOTE_ID_EXTRA;

/**
 * PagerActivity class.
 */
public class PagerActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {

    private int mCurrentNoteId;

    public static void navigate(Context context, int noteId) {
        Intent intent = new Intent(context, PagerActivity.class);
        intent.putExtra(NOTE_ID_EXTRA, noteId);
        context.startActivity(intent);
    }

    private FloatingActionButton mFab;
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);
        mCurrentNoteId = getIntent().getIntExtra(NOTE_ID_EXTRA, 0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mViewPager = (ViewPager) findViewById(R.id.container);
        getSupportLoaderManager().initLoader(0, null, this);

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setVisibility(View.INVISIBLE);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO check null
                CreateEditNoteActivity.editNote(PagerActivity.this, getCurrentNote());
            }
        });

        setToolbarForActivity();
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        // super.onSaveInstanceState(outState);
    }

    @Override
    public final boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pager, menu);
        return true;
    }

    @Override
    public final boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share_twitter) {
            shareOnTwitter();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public final Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,
                DbProviderMetaData.NoteTableMetaData.CONTENT_URI,
                new String[]{DbProviderMetaData.NoteTableMetaData.COLUMN_ID},
                null,
                null,
                null);
    }

    @Override
    public final void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        List<Integer> idList = new ArrayList<>();
        while (cursor.moveToNext()) {
            idList.add(cursor.getInt(cursor.getColumnIndex(
                    DbProviderMetaData.NoteTableMetaData.COLUMN_ID)));
        }
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), idList);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(mCurrentNoteId - 1);
        mFab.setVisibility(View.VISIBLE);
    }

    //TODO implement
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    private void shareOnTwitter() {
        PackageManager pm = getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            pm.getPackageInfo("com.twitter.android", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.setPackage("com.twitter.android");
            waIntent.putExtra(Intent.EXTRA_TEXT, getCurrentNote().getMessage());
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "Twitter not Installed", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private Note getCurrentNote() {
        PagerFragment pagerFragment = (PagerFragment) mSectionsPagerAdapter
                .instantiateItem(mViewPager, mViewPager.getCurrentItem());
        return pagerFragment.getNote();
    }

    private void setToolbarForActivity() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PagerActivity.this, MainActivity.class);
                PagerActivity.this.startActivity(intent);
                PagerActivity.this.finish();
            }
        });
    }

    /**
     * A {@link FragmentStatePagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public static class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private final List<Integer> mNoteIdList;

        SectionsPagerAdapter(FragmentManager fragmentManager, List<Integer> noteIdList) {
            super(fragmentManager);
            mNoteIdList = noteIdList;
        }

        @Override
        public final Fragment getItem(int position) {
            return PagerFragment.newInstance(mNoteIdList.get(position));
        }

        @Override
        public final int getCount() {
            return mNoteIdList.size();
        }

        // This is called when notifyDataSetChanged() is called
        @Override
        public final int getItemPosition(Object object) {
            // refresh all fragments when data set changed
            return PagerAdapter.POSITION_NONE;
        }

    }

}
