package melnikacg.notebook.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import melnikacg.notebook.R;
import melnikacg.notebook.database.DbProviderMetaData;
import melnikacg.notebook.model.Note;
import melnikacg.notebook.ui.adapter.NoteRecyclerViewAdapter;
import melnikacg.notebook.utils.UtilsScreen;

public class MainActivity extends AppCompatActivity
        implements NoteRecyclerViewAdapter.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private NoteRecyclerViewAdapter mNoteRecyclerViewAdapter;

    private static final String EXTRA_SORT_ORDER = "sort_order";
    @Override
    public final Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,
                DbProviderMetaData.NoteTableMetaData.CONTENT_URI,
                null,
                null,
                null,
                args.getString(EXTRA_SORT_ORDER));
    }

    @Override
    public final void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        List<Note> notes = new ArrayList<>();
        for (cursor.moveToLast(); !cursor.isBeforeFirst(); cursor.moveToPrevious()) {
            Note note = Note.cursorToNote(cursor);
            notes.add(note);
        }

        mNoteRecyclerViewAdapter.setItems(notes);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateEditNoteActivity.createNewNote(MainActivity.this);
            }
        });
        Bundle bundle = new Bundle();
        bundle.putString(
                EXTRA_SORT_ORDER, DbProviderMetaData.NoteTableMetaData.DEFAULT_SORT_ORDER_CREATE);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(
                UtilsScreen.getDisplayColumns(this), StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);

        mNoteRecyclerViewAdapter = new NoteRecyclerViewAdapter();
        mNoteRecyclerViewAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(mNoteRecyclerViewAdapter);

        getSupportLoaderManager().initLoader(0, bundle, this);
    }

    @Override
    public final boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public final boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (id == R.id.action_sort) {
            generateSortDialog().show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public final void onBackPressed() {
        finishAffinity();
    }

    @Override
    public final void onItemClick(int noteID) {
        PagerActivity.navigate(this, noteID);
    }

    private AlertDialog generateSortDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        final Map<String, String> sortOrderItems = new HashMap<>();
        sortOrderItems.put(getString(R.string.sort_order_creation),
                DbProviderMetaData.NoteTableMetaData.COLUMN_DATE_CREATE);
        sortOrderItems.put(getString(R.string.sort_order_due),
                DbProviderMetaData.NoteTableMetaData.COLUMN_DATE_REMIND);
        final String[] sortOrderItemNames = sortOrderItems.keySet().toArray(
                new String[sortOrderItems.size()]);

        builder.setTitle(getString(R.string.dialog_sort_title));
        //TODO Get current sort from pref
        builder.setSingleChoiceItems(sortOrderItemNames, 0, null);

        //TODO refactor onClick
        builder.setPositiveButton(R.string.dialog_sort_asc, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                ListView list = ((AlertDialog) dialog).getListView();
                String checkedItem = (String) list.getAdapter()
                        .getItem(list.getCheckedItemPosition());
                Bundle bundle = new Bundle();
                bundle.putString(EXTRA_SORT_ORDER, sortOrderItems.get(checkedItem) + " ASC");
                getSupportLoaderManager().restartLoader(0, bundle, MainActivity.this);
            }
        });
        builder.setNegativeButton(R.string.dialog_sort_desc, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                ListView list = ((AlertDialog) dialog).getListView();
                String checkedItem = (String) list.getAdapter()
                        .getItem(list.getCheckedItemPosition());
                Bundle bundle = new Bundle();
                bundle.putString(EXTRA_SORT_ORDER, sortOrderItems.get(checkedItem) + " DESC");
                getSupportLoaderManager().restartLoader(0, bundle, MainActivity.this);
            }
        });

        builder.setCancelable(true);
        return builder.create();
    }

}
