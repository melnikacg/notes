package melnikacg.notebook.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

import melnikacg.notebook.database.DbProviderMetaData.NoteTableMetaData;

import static melnikacg.notebook.database.DbProviderMetaData.NoteTableMetaData.COLUMN_MESSAGE;

/**
 * Note model class.
 */
public final class Note implements Parcelable {

    public static final String NOTE_ID_EXTRA = "melnikacg.notebook.Identifier";
    private final String mTitle;
    private final String mMessage;
    private final int mNoteId;
    private long mDateCreate;

    private long mDateRemind;

    private Note(String title, String message, int noteId, long dateCreate, long dateRemind) {
        this.mTitle = title;
        this.mMessage = message;
        this.mNoteId = noteId;
        this.mDateCreate = dateCreate;
        this.mDateRemind = dateRemind;
    }

    private Note(Parcel parcel) {
        mTitle = parcel.readString();
        mMessage = parcel.readString();
        mDateCreate = parcel.readLong();
        mDateRemind = parcel.readLong();
        mNoteId = parcel.readInt();
    }

    public long getDateRemind() {
        return mDateRemind;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getMessage() {
        return mMessage;
    }

    public int getNoteId() {
        return mNoteId;
    }

    @Override
    public String toString() {
        return "ID: " + mNoteId + " Title: " + mTitle + " Message: " + mMessage
                + " Date create: " + mDateCreate + " Date remind: " + mDateRemind;
    }

    /**
     * The method will convert the cursor to the new model assembled.
     *
     * @param cursor cursor.
     * @return assembled {@link Note} model instance.
     */
    public static Note cursorToNote(Cursor cursor) {
        return new Note(cursor.getString(cursor.getColumnIndex(NoteTableMetaData.COLUMN_TITLE)),
                cursor.getString(cursor.getColumnIndex(NoteTableMetaData.COLUMN_MESSAGE)),
                cursor.getInt(cursor.getColumnIndex(NoteTableMetaData.COLUMN_ID)),
                cursor.getLong(cursor.getColumnIndex(NoteTableMetaData.COLUMN_DATE_CREATE)),
                cursor.getLong(cursor.getColumnIndex(NoteTableMetaData.COLUMN_DATE_REMIND)));
    }

    /**
     * This is used for new notes.
     *
     * @param title   note id.
     * @param message title.
     * @return assembled {@link ContentValues}.
     */
    public static ContentValues createNoteCv(String title, String message, long dateRemind) {
        ContentValues values = new ContentValues();
        values.put(NoteTableMetaData.COLUMN_TITLE, title);
        values.put(COLUMN_MESSAGE, message);
        values.put(NoteTableMetaData.COLUMN_DATE_CREATE, Calendar.getInstance().getTimeInMillis());
        values.put(NoteTableMetaData.COLUMN_DATE_REMIND, dateRemind);
        return values;
    }

    /**
     * This is used for existing notes update. Creation date is taken current.
     *
     * @param noteId     note id.
     * @param title      title.
     * @param message    message.
     * @param remindDate reminder date.
     * @return assembled {@link ContentValues}.
     */
    public static ContentValues createNoteCv(int noteId, String title, String message, long remindDate) {
        ContentValues values = new ContentValues();
        values.put(NoteTableMetaData.COLUMN_ID, noteId);
        values.put(NoteTableMetaData.COLUMN_TITLE, title);
        values.put(COLUMN_MESSAGE, message);
        values.put(NoteTableMetaData.COLUMN_DATE_CREATE, Calendar.getInstance().getTimeInMillis());
        values.put(NoteTableMetaData.COLUMN_DATE_REMIND, remindDate);
        return values;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(mTitle);
        parcel.writeString(mMessage);
        parcel.writeLong(mDateCreate);
        parcel.writeLong(mDateRemind);
        parcel.writeInt(mNoteId);
    }

    /**
     * Use parcelable creator for {@link Note} instance forwarding.
     */
    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {

        public Note createFromParcel(Parcel parcel) {
            return new Note(parcel);
        }

        public Note[] newArray(int size) {
            return new Note[size];
        }
    };
}
